
# coding: utf-8

# ### The Autoregressive Process
# Autoregressive Processes is the one in which only the first p of the weights are nonzero. The model may be written:
# 
# $$  \tilde{z_t} = \phi_1 \tilde{z}_{t-1} \; + \; \phi_2 \tilde{z}_{t-2}\; + \; ... \; + \; \phi_p \tilde{z}_{t-p} \; + \; \varepsilon_t $$
# 
# The moving average process can be written as:
# 
# $$  \tilde{z_t} = \varepsilon_t - \theta_1 a_{t-1} \; - \; \theta_2 a_{t-2}\; - \; ... \; - \; \theta_q a_{t-q} $$
# 
# The Gini Autocorrelation Function (GACF) is defined by:
# 
# $$  GACF(Y_t,Y_{t-s}) = \frac{COV(Y_t,F(Y_{t-s}))}{COV(Y_t,F(Y_t))} $$
# <br>
# and
# $$  GACF(Y_{t-s},Y_t) = \frac{COV(Y_{t-s},F(Y_t))}{COV(Y_{t-s},F(Y_{t-s}))} $$
# 
# <br>
# The Gini Partial Autocorrelation Function is:
# 
# $$ \phi_{k+1,k+1} = \frac{\hat{\rho}_{k+1} - \sum_{j = 1}^{k} \hat{\phi}_{kj} \hat{\rho}_{k+1-j}}
# {1 - \sum_{j = 1}^{k} \hat{\phi}_{kj} \hat{\rho}_{j}} $$
# 
# and
# 
# $$ \hat{\phi}_{k+1 , j} = \hat{\phi}_{kj} - \hat{\phi}_{k+1,k+1} \hat{\phi}_{k, k+1-j}  $$
# 
# In the case of this code the two first terms were calculated by:
# 
# $$ \hat{\phi}_{1,1} =  \hat{\rho}_{1} $$
# 
# and
# 
# $$ \hat{\phi}_{2,2} =  \frac{\hat{\rho}_{2} - \hat{{\rho}_{1}}^2 } {1 - \hat{{\rho}_{1}}^2} $$
# 
# The Parameters for the ARMA Model are calculate based on the same principle use to the Multiple Regressions (MR), minimizind the Gini of the Error Term as follow:
# 
# $$ min \; \; Cov(e, R(e))  $$
# 
# where the Error is calculated regressing the ARMA parametes:
# 
# $$ \epsilon_t  = Y_t - \phi_0 - \phi_1 Y_{t-1} \; - \;  ... \; + \; \theta_1 \epsilon_{t-1} + \theta_2 \epsilon_{t-2} ... $$

# In[ ]:


from pandas import read_excel, DataFrame, Series, ExcelWriter
import numpy as np
import statsmodels.api as sm
import warnings
from statsmodels.tsa.stattools import pacf, acf
import matplotlib.pyplot as plt
warnings.filterwarnings("ignore") # specify to ignore warning messages
from collections import OrderedDict
import shutil
from re import findall

get_ipython().magic('matplotlib inline')

sOperator = 'Forward'
sGoodOrBad = 'Bad'

import modGini_Standard as gini

# sDirectory = r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Data\Synthetic_Series'
sDirectory = r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Data\Synthetic_{}_{}Results'.format(sOperator, sGoodOrBad)
# sDestGood = r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Data\Synthetic_{}_GoodResults'.format(sOperator)
# sDestBad  = r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Data\Synthetic_{}_BadResults'.format(sOperator)

sFiles = gini.FuncFindFiles(sDirectory, '.xlsx')

# General Definitions which will be used across the Series
dBestParams = DataFrame(columns=['Série', 'Ordem', 'Ordem do Modelo',
                                 'Clássico', 'Gini Min', 'Gini SP', 'Arquivo'])

dNormalityTeste = DataFrame(columns=['Série', 'Teste Clássico', 'P-value Clássico', 'Normal_Classico',
                                              'Teste Gini Min', 'P-Valor Gini Min', 'Normal_GiniMin',
                                              'Teste Gini SP',  'P-Valor Gini SP',  'Normal_GiniSP'])
                        
dErrorMAPE = DataFrame(columns=['Série', 'Clássico', 'Gini SP', 'Gini Min', 'Arquivo'])
dErrorMSD  = DataFrame(columns=['Série', 'Clássico', 'Gini SP', 'Gini Min', 'Arquivo'])
dErrorMAE  = DataFrame(columns=['Série', 'Clássico', 'Gini SP', 'Gini Min', 'Arquivo'])
dErrorMSE  = DataFrame(columns=['Série', 'Clássico', 'Gini SP', 'Gini Min', 'Arquivo'])

GiniCoeff = DataFrame(columns=['Série', 'Clássico', 'Gini SP','Gini Min', 
                               'SP/Min', 'Clássico/SP', 'Clássico/Min',
                               'Arquivo'])

GiniCorrelationsOne = DataFrame(columns=['Série', 'Clássico', 'Gini SP', 'Gini Min',
                                         'SP/Min', 'Clássico/SP', 'Clássico/Min',
                                         'Arquivo'])
GiniCorrelationsTwo = DataFrame(columns=['Série', 'Clássico', 'Gini SP', 'Gini Min',
                                         'SP/Min', 'Clássico/SP', 'Clássico/Min',
                                         'Arquivo'])

for j in range(0, len(sFiles)):

#     gini.FuncPrintHeader(sFiles[j])

    dData = read_excel(sFiles[j])
    sCol  = dData.columns.values[0]

    #==================            IMPORTANTE NOTES       ============================#

    # THE LOCATION PARAMETER (PHI ZERO) MUST BE OBTAINED BY THE MINIMIZATION METHOD, IN THE CASE MINIMIZE THE GMD BASED ON THE COEFFICIENTS
    # CREATING THE AR MODEL, WHEN ESTABLISH THIS PARAMETER, THEN WHEN RUN THE MODEL AGAIN AND STORES THE ERROR METRICS
    # I STILL NEED TO IMPLEMENT THE CALCULATIONS OF MAPE, MAD AND MSE TO ACESS THE FORECASTS QUALITY.

    #========================== AUTOCOVARIANCE =================================#

    # The process is Yt = the time series from [lag:end]
    # Yt-s is the cumulative distribution from Y[i-lag:lastvalue-lag]
    # These 2 vectors are the same lenght and define the coefficients from the autoregressive process
    # Teta zero is (Sum(Yt - Y_bar)**2)/len(dData)

    iNLags = 20

    siACF, siPACF, siGACF_1, siGACF_2, siGPACF_1, siGPACF_2, siGini_Covs_1, siGini_Covs_2 = ({} for dic in range(8))

    #--------------------------------------------------------------------------------
    #ACF, PACF --> Stores the Classical Autocorrelation and Partial ACF.
    #siGACF, GPACF, Gini_Covs --> Stores the Autocorrelation, Partial ACF and the Covariances for both GINI indexes
    #dct --> dictionary to store the results of the AIC and Param to every ARIMA test
    #Forecasts --> Stores the Forecasts from the Gini 
    #MSE, MSD, MAE, MAPE --> Error metrics to analyze the results
    #--------------------------------------------------------------------------------

    #===================== CREATING THE DATAFRAMES TO STORES RESULTS =============================#

    dMatrix_ranks = DataFrame(index=dData.index.values)
    dMatrix_GAR_Coef1, dMatrix_GAR_Coef2 = (DataFrame(index=np.arange(1,iNLags+1,1),columns=[sCol]) for i in range(2))
    dData_central = DataFrame()

    #--------------------------------------------------------------------------------
    #dMatrix_ranks --> Stores the Centralized and Differenced Data (If necessary) and their Ranks
    #dMatrix_GAR_Coef1, 2 --> Stores the Partial Correlations which characterize the AR(p) Coefficients
    #dData_central --> Stores the Centralized Data
    #dFits --> Stores the Residuals and the Fitted Values from the ARIMA Models
    #--------------------------------------------------------------------------------

    #============== MATRIX OF RANKS BASED ON THE DICKEY-FULLER STATIONARITY TEST ==============#

    dMatrix_ranks = gini.FuncCreateMatrixRank(dMatrix_ranks, dData,sCol)

    dMatrix_ranks['CDF_' + sCol] = ((dMatrix_ranks[sCol].rank(method='average',na_option='top')/len(dMatrix_ranks)) - 1/2)
    dData_central[sCol] = dData[sCol] - dData[sCol].mean()

    #================================= SIGNIFICANCE LEVEL (5%) =================================#

    significance = (+1.96)/np.sqrt(len(dData))  #Significance levels for 5% Confidence

    siACF[sCol], siPACF[sCol], siGACF_1[sCol], siGACF_2[sCol], siGini_Covs_1[sCol], siGini_Covs_2[sCol] = ([] for i in range(6))

    nCov = 4 * np.cov(dMatrix_ranks[sCol], (dMatrix_ranks[sCol].rank(method='average',na_option='top')/len(dMatrix_ranks[sCol])))[0][1]

    siGini_Covs_1[sCol].append(nCov)
    siGini_Covs_2[sCol].append(nCov)

        #========================= AUTOCORRELATION gini.FuncTION  =================# 

    siGACF_1 = gini.FuncsiGACFOne(siGACF_1 = siGACF_1, dMatrix_ranks=dMatrix_ranks, sCol=sCol, iNLags=iNLags, siGini_Covs=siGini_Covs_1)[0]
    siGACF_2 = gini.FuncsiGACFTwo(siGACF_2 = siGACF_2, dMatrix_ranks=dMatrix_ranks, sCol=sCol, iNLags=iNLags, siGini_Covs=siGini_Covs_2)[0]

    #======================= AUTO AND PARTIAL CORRELATION gini.FuncTIONS (CLASSICAL)- ACF AND PACF ======================#

    siACF[sCol].append(acf(dMatrix_ranks[sCol], nlags=iNLags))            
    siPACF[sCol].append(pacf(dMatrix_ranks[sCol], nlags=iNLags, method='ols'))

    #======================= ESTIMATION OF THE GINI'S PARTIAL AUTOCORRELATIONS =============================#

    siCoef_Gar_1 = gini.FuncCoefGini(sCol, siGACF_1, siGACF_2)[0]
    siCoef_Gar_2 = gini.FuncCoefGini(sCol, siGACF_1, siGACF_2)[1]

    siPartial1, siPartial2 = (DataFrame(index=np.arange(1,iNLags+1,1),columns=np.arange(1,iNLags+1,1)) for p in range(2)) #DataFrame with all the Autocorrelations
    siPartial1.fillna('',inplace=True); siPartial2.fillna('',inplace=True)

    for i in range(1,3):  # Insert PACF Lag 1 and 2 into the DataFrame
        siPartial1[i][i] = siCoef_Gar_1[i-1]
        siPartial2[i][i] = siCoef_Gar_2[i-1]

    siPartial1 = gini.FuncCalcTheGiniCoefs(iNLags, siGACF = siGACF_1, sCol = sCol, siPartial = siPartial1, siReverse = siGACF_1[sCol][::-1])
    siPartial2 = gini.FuncCalcTheGiniCoefs(iNLags, siGACF = siGACF_2, sCol = sCol, siPartial = siPartial2, siReverse = siGACF_2[sCol][::-1])

    for coefs in range(3, iNLags+1):
        siCoef_Gar_1.append(siPartial1[coefs][coefs]) 
        siCoef_Gar_2.append(siPartial2[coefs][coefs]) 

    dMatrix_GAR_Coef1[sCol] = siCoef_Gar_1
    dMatrix_GAR_Coef2[sCol] = siCoef_Gar_2

        #-----------------------------------------------------------------------#
 #---------------              SELECTING THE BEST ARIMA MODEL                  -------------#
        #-----------------------------------------------------------------------#

    #siForecasts   --> Stores the Forecasts from the ARIMA
    #dBestParam --> #Stores the parameters with the lowest AIC
    #dFits       --> Store the Fitted values for the ARIMA Models

    dBestParam, siForecasts, dFits, siParams   = gini.FuncFindBestARIMA(iPmin = 1, iPmax = 5, sCol = sCol, dMatrix_ranks = dMatrix_ranks,
                                                                       dData_central = dData_central)

                #-----------------------------------------------------------------------#
    #--------------- GINI'S ARIMA (GARIMA) FOR THE SAME CHOSEN PARAMETERS  ----------------------#
                #-----------------------------------------------------------------------#           

    if sOperator == 'Forward':
        siGini_Parameters = dMatrix_GAR_Coef1
    if sOperator == 'Backward':
        siGini_Parameters = dMatrix_GAR_Coef2

    dForecasts, siAR_Coeff_M, siResidualsGini = gini.FuncGiniPureAR(dData_central = dData_central, sCol = sCol, dBestParam = dBestParam,
                                                                    dMatrix_GAR_Coef1 = siGini_Parameters)

                #=====================================================#
    #================== PURE AUTOREGRESSIVE SEMI-PARAMETRIC =================#
                #======================================================#

    dForecasts_SP, siAR_Coeff_SP, siResidualsGiniSP = gini.FuncGiniARSemiParametric(sCol = sCol, dData_central = dData_central, dBestParam = dBestParam,
                                                                                    dMatrix_GAR_Coef1 = siGini_Parameters)

    #------------------------------  CREATING A TABLE WITH THE PARAMETERS VALUES ------------------------------------#
    dBestParams = dBestParams.append({'Série': sCol.split("_")[0] + '_' + ' '.join(sCol.split("_Ordem")[0].split("_")[1:]) + ', p = ' + sCol.split("_Ordem_")[1][0],
                                      'Ordem': sFiles[j].split('.')[0][-1],
                                      'Ordem do Modelo': dBestParam[0][0],
                                      'Clássico': np.around(siParams, 2), 
                                      'Gini Min': np.around(siAR_Coeff_M.values,2), 
                                      'Gini SP': np.around(siAR_Coeff_SP.values,2),
                                      'Arquivo': sFiles[j]}, ignore_index=True)

    #------------------------------  ERROR METRICS ------------------------------------#

    dErrorMetrics = gini.FuncErrorMetrics(sCol = sCol, dData_central = dData_central, dFits = dFits,
                                          dForecasts = dForecasts, dBestParam = dBestParam, dForecasts_SP = dForecasts_SP)

    dErrorMAPE = dErrorMAPE.append({'Série': sCol.split("_")[0] + '_' + ' '.join(sCol.split("_Ordem")[0].split("_")[1:]) + ', p = ' + sCol.split("_Ordem_")[1][0], 
                                    'Clássico': np.round_(dErrorMetrics.loc['MAPE']['ARIMA_' + sCol][0], 3),
                                    'Gini SP': np.round_(dErrorMetrics.loc['MAPE']['GINI_SP_'+ sCol][0], 3),
                                    'Gini Min': np.round_(dErrorMetrics.loc['MAPE']['GINI_M_'+ sCol][0], 3),
                                    'Arquivo': sFiles[j]}, ignore_index=True)

    dErrorMSD  = dErrorMSD.append({'Série': sCol.split("_")[0] + '_' + ' '.join(sCol.split("_Ordem")[0].split("_")[1:]) + ', p = ' + sCol.split("_Ordem_")[1][0], 
                                    'Clássico': np.round_(dErrorMetrics.loc['MSD']['ARIMA_' + sCol][0], 3),
                                    'Gini SP': np.round_(dErrorMetrics.loc['MSD']['GINI_SP_'+ sCol][0], 3),
                                    'Gini Min': np.round_(dErrorMetrics.loc['MSD']['GINI_M_'+ sCol][0], 3),
                                    'Arquivo': sFiles[j]}, ignore_index=True)

    dErrorMAE  = dErrorMAE.append({'Série': sCol.split("_")[0] + '_' + ' '.join(sCol.split("_Ordem")[0].split("_")[1:]) + ', p = ' + sCol.split("_Ordem_")[1][0], 
                                    'Clássico': np.round_(dErrorMetrics.loc['MAE']['ARIMA_' + sCol][0], 3),
                                    'Gini SP': np.round_(dErrorMetrics.loc['MAE']['GINI_SP_'+ sCol][0], 3),
                                    'Gini Min': np.round_(dErrorMetrics.loc['MAE']['GINI_M_'+ sCol][0], 3),
                                    'Arquivo': sFiles[j]}, ignore_index=True)

    dErrorMSE  = dErrorMSE.append({'Série': sCol.split("_")[0] + '_' + ' '.join(sCol.split("_Ordem")[0].split("_")[1:]) + ', p = ' + sCol.split("_Ordem_")[1][0], 
                                    'Clássico': np.round_(dErrorMetrics.loc['MSE']['ARIMA_' + sCol][0], 3),
                                    'Gini SP': np.round_(dErrorMetrics.loc['MSE']['GINI_SP_'+ sCol][0], 3),
                                    'Gini Min': np.round_(dErrorMetrics.loc['MSE']['GINI_M_'+ sCol][0], 3),
                                    'Arquivo': sFiles[j]}, ignore_index=True)

                #============================================================================#
    #=================== CALCULATING GINI COEFFICIENT AND CORRELATIONS TO RESIDUALS RESIDUALS =======================#
                #===========================================================================#
    nClassicGiniCoeff = gini.fGiniCoefficient(dFits['Res_'+sCol]) 
    nGiniSPGiniCoeff = gini.fGiniCoefficient(siResidualsGiniSP['Residual'+sCol]) 
    nGiniMinGiniCoeff = gini.fGiniCoefficient(siResidualsGini['Residual'+sCol]) 
    GiniCoeff = GiniCoeff.append({'Série': sCol.split("_")[0] + '_' + ' '.join(sCol.split("_Ordem")[0].split("_")[1:]) + ', p = ' + sCol.split("_Ordem_")[1][0],
                                  'Clássico': nClassicGiniCoeff, 
                                  'Gini SP': nGiniSPGiniCoeff, 
                                  'Gini Min': nGiniMinGiniCoeff,
                                  'SP/Min': abs(nGiniSPGiniCoeff/nGiniMinGiniCoeff),
                                  'Clássico/SP': abs(nClassicGiniCoeff/nGiniSPGiniCoeff),
                                  'Clássico/Min': abs(nClassicGiniCoeff/nGiniMinGiniCoeff),
                                  'Arquivo':sFiles[j]}, ignore_index=True)

    size = min(len(dFits['Res_'+sCol]), len(dMatrix_ranks[sCol]))
    nClassicGiniCorrOne = gini.FuncGiniCorrOne(dFits['Res_'+sCol][:size], dMatrix_ranks[sCol][:size])
    
    size2 = min(len(siResidualsGiniSP['Residual'+sCol]), len(dMatrix_ranks[sCol]))
    nGiniSPGiniCorrOne = gini.FuncGiniCorrOne(siResidualsGiniSP['Residual'+sCol][:size2], dMatrix_ranks[sCol][:size2])
    
    size3 = min(len(siResidualsGiniSP['Residual'+sCol]), len(dMatrix_ranks[sCol]))
    nGiniMinGiniCorrOne = gini.FuncGiniCorrOne(siResidualsGini['Residual'+sCol][:size3], dMatrix_ranks[sCol][:size3])
    
    GiniCorrelationsOne = GiniCorrelationsOne.append({'Série': sCol.split("_")[0] + '_' + ' '.join(sCol.split("_Ordem")[0].split("_")[1:]) + ', p = ' + sCol.split("_Ordem_")[1][0], 
                                                      'Clássico': nClassicGiniCorrOne, 
                                                      'Gini SP': nGiniSPGiniCorrOne, 
                                                      'Gini Min': nGiniMinGiniCorrOne,
                                                      'SP/Min': abs(nGiniSPGiniCorrOne/nGiniMinGiniCorrOne),
                                                      'Clássico/SP': abs(nClassicGiniCorrOne/nGiniSPGiniCorrOne),
                                                      'Clássico/Min': abs(nClassicGiniCorrOne/nGiniMinGiniCorrOne),
                                                      'Arquivo': sFiles[j]}, ignore_index=True)
    
    nClassicGiniCorrTwo = gini.FuncGiniCorrTwo(dFits['Res_'+sCol][:size], dMatrix_ranks[sCol][:size])
    nGiniSPGiniCorrTwo = gini.FuncGiniCorrTwo(siResidualsGiniSP['Residual'+sCol][:size2], dMatrix_ranks[sCol][:size2])
    nGiniMinGiniCorrTwo = gini.FuncGiniCorrTwo(siResidualsGini['Residual'+sCol][:size3], dMatrix_ranks[sCol][:size3])
    GiniCorrelationsTwo = GiniCorrelationsTwo.append({'Série': sCol.split("_")[0] + '_' + ' '.join(sCol.split("_Ordem")[0].split("_")[1:]) + ', p = ' + sCol.split("_Ordem_")[1][0],
                                                      'Clássico': nClassicGiniCorrTwo, 
                                                      'Gini SP': nGiniSPGiniCorrTwo, 
                                                      'Gini Min': nGiniMinGiniCorrTwo,
                                                      'SP/Min': abs(nGiniSPGiniCorrTwo/nGiniMinGiniCorrTwo),
                                                      'Clássico/SP': abs(nClassicGiniCorrTwo/nGiniSPGiniCorrTwo),
                                                      'Clássico/Min': abs(nClassicGiniCorrTwo/nGiniMinGiniCorrTwo),
                                                      'Arquivo': sFiles[j]}, ignore_index=True)
    gini.FuncGiniCorrTwo
                     #=====================================================#
    #=================== NORMALITY TEST FOR THE RESIDUALS AND PLOTS =======================#
                #======================================================#

    sSerieName = findall('LogNormal|Burr|Pareto|Weibull|Bimodal', sCol.split("_")[0])[0]
    
# #     gini.FuncDataDistribution(dData = dData_central[sCol], bins = 50)
#     print('')
#     print('')

    gini.FuncPlotAutoCorrelations(dMatrix_ranks = dMatrix_ranks[sCol], iNLags = iNLags, sCol = sCol,
                                  siGACF_1 = siGACF_1[sCol], siGACF_2 = siGACF_2[sCol], sOperator=sOperator, sGoodOrBad = sGoodOrBad)
    
    gini.FuncPlotPartialAutocorr(dMatrix_ranks = dMatrix_ranks[sCol], iNLags = iNLags, sCol = sCol,
                                 dMatrix_GAR_Coef1 = dMatrix_GAR_Coef1[sCol],
                                 dMatrix_GAR_Coef2 = dMatrix_GAR_Coef2[sCol], sOperator=sOperator, sGoodOrBad = sGoodOrBad)

# #     print('')
# #     print('')
# #     print('')
#     print('-----------------------------------------------------------------------------------')
#     print("-------------                   MODEL'S RESULTS               ---------------------")
#     print('-----------------------------------------------------------------------------------')
#     print('')
# #     print('---------------         AUTO REGRESSIVE MODEL RESULTS         ----------------')
# #     print('')

    siClassic_Normality, sNormClass = gini.FuncNormalityResiduals(dFits = dFits['Res_'+sCol], sCol = sCol)

    gini.FuncPlotProbQQ(siResidual = dFits['Res_'+sCol], siFitted = dFits['Fitted_'+sCol], 
                        dData = dData_central[sCol], sCol = sCol, sMethod = 'Método Clássico', 
                        sOperator=sOperator, sGoodOrBad = sGoodOrBad)


# #     print('------------------       GINI MINIMIZATION MODEL RESULTS        ----------------')
# #     print('')

    siGiniM_Normality, sNormGiniM = gini.FuncNormalityResiduals(dFits = dForecasts[sCol][dBestParam[0][0]:], sCol = sCol)

    gini.FuncPlotProbQQ(siResidual = siResidualsGini['Residual'+sCol], siFitted = siResidualsGini['Residual'+sCol],
                        dData = dData_central[sCol][dBestParam[0][0]:], sCol = sCol, sMethod = 'Gini Minimização', sOperator=sOperator, sGoodOrBad = sGoodOrBad)

# #     print('')
# #     print('---------------    GINI SEMI-PARAMETRIC MODEL RESULTS     ----------------')
# #     print('')

    siGiniSP_Normality, sNormGiniSP = gini.FuncNormalityResiduals(dFits = dForecasts_SP[sCol][dBestParam[0][0]:], sCol = sCol)

    gini.FuncPlotProbQQ(siResidual = siResidualsGiniSP['Residual'+sCol], siFitted = siResidualsGiniSP['Residual'+sCol],
                        dData = dData_central[sCol][dBestParam[0][0]:], sCol = sCol, sMethod = 'Gini Semi-Paramétrico', sOperator=sOperator, sGoodOrBad = sGoodOrBad)


    dNormalityTeste = dNormalityTeste.append({'Série': 'Distribuição ' + sCol.split("_")[0] + ' com ' + ' '.join(sCol.split("_Ordem")[0].split("_")[1:]) + ', p = ' + sCol.split("_Ordem_")[1][0], 
                                               'Teste Clássico': round(siClassic_Normality[0],2), 'P-value Clássico': round(siClassic_Normality[1],2), 'Normal_Classico': sNormClass,
                                               'Teste Gini Min': round(siGiniM_Normality[0], 2),  'P-Valor Gini Min': round(siGiniM_Normality[1],2),   'Normal_GiniMin':  sNormGiniM,                     
                                               'Teste Gini SP':  round(siGiniSP_Normality[0], 2), 'P-Valor Gini SP':  round(siGiniSP_Normality[1], 2), 'Normal_GiniSP':   sNormGiniSP},
                                               ignore_index = True)

    print('')
#     print('-------        COMPARING: ERROR METRICS       --------')
#     print('')
#     print('AUTO REGRESSIVE:      MAE: {}, MSE: {}, MAPE: {}, MSD: {} '.format(np.round_(dErrorMetrics.loc['MAE']['ARIMA_' + sCol][0], 1), 
#                                                                               np.round_(dErrorMetrics.loc['MSE']['ARIMA_' + sCol][0], 1),
#                                                                               np.round_(dErrorMetrics.loc['MAPE']['ARIMA_'+ sCol][0], 1),
#                                                                               np.round_(dErrorMetrics.loc['MSD']['ARIMA_' + sCol][0], 1)))

#     print('GINI MINIMIZATION:    MAE: {}, MSE: {}, MAPE: {}, MSD: {} '.format(np.round_(dErrorMetrics.loc['MAE']['GINI_M_' + sCol][0], 3),
#                                                                               np.round_(dErrorMetrics.loc['MSE']['GINI_M_' + sCol][0], 3),
#                                                                               np.round_(dErrorMetrics.loc['MAPE']['GINI_M_'+ sCol][0], 3),
#                                                                               np.round_(dErrorMetrics.loc['MSD']['GINI_M_' + sCol][0], 3)))

#     print('GINI SEMI PARAMETRIC: MAE: {}, MSE: {}, MAPE: {}, MSD: {} '.format(np.round_(dErrorMetrics.loc['MAE']['GINI_SP_' + sCol][0], 3),
#                                                                               np.round_(dErrorMetrics.loc['MSE']['GINI_SP_' + sCol][0], 3),
#                                                                               np.round_(dErrorMetrics.loc['MAPE']['GINI_SP_'+ sCol][0], 3),
#                                                                               np.round_(dErrorMetrics.loc['MSD']['GINI_SP_' + sCol][0], 3)))

    print('')
    print('------------------     COMPARING REAL DATA AND THE MODELS RESULTS    ----------------')
    param = ''.join(map(str, [int(s) for s in sCol.split("_")[:-2] if s.isdigit()]))
    plt.rcParams["figure.figsize"] = [25, 12]   

    Series(dForecasts[sCol][dBestParam[0][0]:]).plot(label='Gini Minimização', c ='r')
    Series(dForecasts_SP[sCol][dBestParam[0][0]:]).plot(label='Gini Semi Paramétrico', c ='g')
    dFits['Fitted_'+ sCol][1:].reset_index(drop=True).plot(label='Modelo AR Clássico', c ='b')
    dData_central[sCol].reset_index(drop=True).plot(label='Real Data', c = 'black' )
    plt.legend(['Gini Minimização','Gini Semi Paramétrico', 'AR Clássico', 'Observação'], fontsize = 28)
    plt.title('{}: Observações versus Previsões para uma Série {}, p = {}'.format(sOperator,
                                                                                    sSerieName,
                                                                                    sCol.split("_")[-3]), fontsize = 34)
    plt.ylabel('Valor da observação (normalizado)', fontsize = 26)
    plt.xlabel('Tempo', fontsize = 26)
    plt.xticks(fontsize = 22)
    plt.yticks(fontsize = 22)
    plt.xlim([0, len(dData_central)])
    plt.tight_layout()
    plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\TimeSeries\{}_{}_FittingPlot_{}.png'.format(sGoodOrBad[0], sOperator[0], sCol.split("_")[0] + param))
    plt.show()
    print('')
    print('')
    print('')
    print('')
    del(sCol)

    
writer = ExcelWriter(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Data\Results\{}_{}_Errors.xlsx'.format(sGoodOrBad, sOperator), engine='xlsxwriter')
dBestParams.to_excel(writer, sheet_name= 'Parâmetros')
dNormalityTeste.to_excel(writer, sheet_name= 'Normalidade dos Resíduos')
dErrorMAPE.to_excel(writer, sheet_name='MAPE')
dErrorMSD.to_excel(writer, sheet_name='MSD')
dErrorMAE.to_excel(writer, sheet_name='MAE')
dErrorMSE.to_excel(writer, sheet_name='MSE')
GiniCoeff.to_excel(writer, sheet_name='GiniCoeff')
GiniCorrelationsOne.to_excel(writer, sheet_name='GiniCorrelationsOne')
GiniCorrelationsTwo.to_excel(writer, sheet_name='GiniCorrelationsTwo')
writer.save()

# Analyzes the models result and get the ones where the Gini performed better and the ones the classical method performed better
# Then it saves then on designed folders to specific runs

if 'sDestGood' not in locals():
    print('')
    print('')
    print('     -------- PROCESS COMPLETE ---------')
else:
    ssGoodResults = []
    ssBadResults = []
    ssDatasets = ['dErrorMAPE','dErrorMSD','dErrorMAE']

    for i in ssDatasets:

        a = list(eval(i)[eval(i)['Clássico'] > eval(i)['Gini SP']]['Arquivo'])
        c = list(eval(i)[eval(i)['Clássico'] < eval(i)['Gini SP']]['Arquivo'])
        for k in range(len(a)):
            ssGoodResults.append(a[k])
        for g in range(len(c)):
            ssBadResults.append(c[g])

        b = list(eval(i)[eval(i)['Clássico'] > eval(i)['Gini Min']]['Arquivo'])
        d = list(eval(i)[eval(i)['Clássico'] < eval(i)['Gini Min']]['Arquivo'])
        for l in range(len(b)):
            ssGoodResults.append(b[l])
        for m in range(len(d)):
            ssBadResults.append(d[m])

    ssBadResults  = DataFrame(ssBadResults)
    ssGoodResults = DataFrame(ssGoodResults)

    ssGood = ssGoodResults[0].value_counts()
    ssBad = ssBadResults[0].value_counts()

    for filename in ssGood[ssGood >= 4].index:
    #     print(filename)
        shutil.copy(filename, sDestGood)

    for filename in ssBad[ssBad >= 5].index:
    #     print(filename)
        shutil.copy(filename, sDestBad)

    print('')
    print('')
    print('     -------- GOOD AND BAD SAMPLES DEFINED - PROCESS COMPLETE ---------')

