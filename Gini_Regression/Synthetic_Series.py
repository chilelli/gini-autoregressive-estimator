
# coding: utf-8

# In[1]:


import numpy as np
import pandas as pd
from scipy.stats import burr
import statsmodels.api as sm
import random
import matplotlib.pyplot as plt
from seaborn import distplot
from re import findall


# ### PARETOS DISTRIBUTIONS

# In[2]:


a = np.arange(2, 12, 3)
m = 2  # shape and mode

DistPareto = {}
for i in range(len(a)):
    dPareto = (np.random.pareto(a[i], 200) + 1) * m
    DistPareto['Pareto' + str(a[i]) + '_' + str(m)] = []
    DistPareto['Pareto' + str(a[i]) + '_' + str(m)].append(dPareto)

    plt.rcParams["figure.figsize"] = [18, 5]

    distplot(DistPareto['Pareto' + str(a[i]) + '_' + str(m)], bins =  100,
             kde_kws={"color": "k", "lw": 5, "label": "Melhor Ajuste", 'linestyle': '--'},
             hist_kws={"histtype": "bar", "linewidth": 4, "alpha": 1, "color": "slategray", "rwidth": 0.6})
    plt.legend(fontsize = 16)
#     plt.title('Histograma para a distribuição {} com fator de forma {} e escala {}'.format('Pareto', str(a[i]), str(m)), fontsize= 22)
    plt.ylabel('Frequência (%)', fontsize = 20)
    plt.xlabel('Valor', fontsize = 20)
    plt.xticks(fontsize = 18)
    plt.yticks(fontsize = 18)
    plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\Distribution and Series\D_Pareto' + str(a[i]).replace(".",'-') + '_' + str(m).replace(".",'-') + '.png')
    plt.show()
    
dict_of_Paretos = {k: pd.DataFrame(v) for k,v in DistPareto.items()}
Paretos = pd.concat(dict_of_Paretos, axis = 0).transpose()
Paretos.columns = Paretos.columns.levels[0]


# ### LOG-NORMAL DISTRIBUTIONS

# In[3]:


mu = [3., 4.]
sigma = [0.9, 1.]

DistLogNormal = {}
for j in range(len(mu)): 
    for i in range(len(sigma)):
        dLogNormal = np.random.lognormal(mu[j], sigma[i], 200)
        DistLogNormal['LogNormal' + str(mu[j]) + '_' + str(sigma[i])] = []
        DistLogNormal['LogNormal' + str(mu[j]) + '_' + str(sigma[i])].append(dLogNormal)
        
        plt.rcParams["figure.figsize"] = [18, 5]

        distplot(DistLogNormal['LogNormal' + str(mu[j]) + '_' + str(sigma[i])], bins =  100,
             kde_kws={"color": "k", "lw": 5, "label": "Melhor Ajuste", 'linestyle': '--'},
             hist_kws={"histtype": "bar", "linewidth": 4, "alpha": 1, "color": "slategray", "rwidth": 0.6});
        plt.legend(fontsize = 16)
#         plt.title('Histograma para a distribuição {} com parâmetro de localização {} e escala {}'.format('Log-Normal', str(mu[j]), str(sigma[i])), fontsize= 22)
        plt.ylabel('Frequência (%)', fontsize = 20)
        plt.xlabel('Valor', fontsize = 20)
        plt.xticks(fontsize = 18)
        plt.yticks(fontsize = 18)
        plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\Distribution and Series\D_LogNormal' + str(int(mu[j])).replace(".",'') + '_' + str(sigma[i]).replace(".",'') + '.png')
        plt.show()
        
dict_of_LogNormal = {k: pd.DataFrame(v) for k,v in DistLogNormal.items()}
LogNormal = pd.concat(dict_of_LogNormal, axis = 0).transpose()
LogNormal.columns = LogNormal.columns.levels[0]


# ### WEIBULL DISTRIBUTIONS

# In[4]:


b = np.arange(8, 12)
d = np.arange(0.3, 0.8, 0.1)
x = np.arange(1, 201.)/100

def weib(x, delta, beta):
    return (beta / delta) * (x / delta)**(beta - 1) * np.exp(-(x / delta)**beta) + 1

DistWeibull = {}
i = 0
for i in range(len(b)):
    dWeibull = []
    for k in x:
        dWeibull.append(weib(k, b[i], d[i]))
        
    DistWeibull['Weibull' + str(round(b[i],1)) + '_' + str(round(d[i], 1))] = []
    DistWeibull['Weibull' + str(round(b[i],1)) + '_' + str(round(d[i], 1))].append(dWeibull)
    
    plt.rcParams["figure.figsize"] = [18, 5]

    distplot(DistWeibull['Weibull' + str(round(b[i],1)) + '_' + str(round(d[i], 1))], bins =  100,
             kde_kws={"color": "k", "lw": 5, "label": "Melhor Ajuste", 'linestyle': '--'},
             hist_kws={"histtype": "bar", "linewidth": 4, "alpha": 1, "color": "slategray", "rwidth": 0.6});
    plt.legend(fontsize = 16)
#     plt.title('Histograma para a distribuição {} com parâmetro de forma {} e escala {}'.format('Weibull', str(round(b[i],1)), str(round(d[i], 1))), fontsize= 22)
    plt.ylabel('Frequência (%)', fontsize = 20)
    plt.xlabel('Valor', fontsize = 20)
    plt.xticks(fontsize = 18)
    plt.yticks(fontsize = 18)
    plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\Distribution and Series\D_Weibull' + str(round(b[i],1)).replace(".",'') + '_' + str(round(d[i],1)).replace(".",'') + '.png')
    plt.show()
    
dict_of_Weibull = {k: pd.DataFrame(v) for k,v in DistWeibull.items()}
Weibull = pd.concat(dict_of_Weibull, axis = 0).transpose()
Weibull.columns = Weibull.columns.levels[0]


# ### BURR DISTRIBUTIONS

# In[5]:


c = np.arange(2, 5, 1) 
d = np.arange(6, 8, 1)

DistBurr = {}
for j in range(len(d)): 
    for i in range(len(c)):
        dBurr = burr.rvs(c[i], d[j], size=200)
        DistBurr['Burr' + str(c[i]) + '_' + str(d[j])] = []
        DistBurr['Burr' + str(c[i]) + '_' + str(d[j])].append(dBurr)
        
        plt.rcParams["figure.figsize"] = [18, 5]

        distplot(DistBurr['Burr' + str(c[i]) + '_' + str(d[j])], bins =  100,
             kde_kws={"color": "k", "lw": 5, "label": "Melhor Ajuste", 'linestyle': '--'},
             hist_kws={"histtype": "bar", "linewidth": 4, "alpha": 1, "color": "slategray", "rwidth": 0.6});
        plt.legend(fontsize = 16)
#         plt.title('Histograma para a distribuição {} com parâmetros de forma {} e {}'.format('Burr',  str(c[i]), str(d[j])), fontsize= 22)
        plt.ylabel('Frequência (%)', fontsize = 20)
        plt.xlabel('Valor', fontsize = 20)
        plt.xticks(fontsize = 18)
        plt.yticks(fontsize = 18)
        plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\Distribution and Series\D_Burr' + str(c[i]).replace(".",'') + '_e_' + str(d[j]).replace(".",'') + '.png')
        plt.show()
        
dict_of_Burr = {k: pd.DataFrame(v) for k,v in DistBurr.items()}
Burr = pd.concat(dict_of_Burr, axis = 0).transpose()
Burr.columns = Burr.columns.levels[0]


# ### MultiModal from a Normal Distribution

# In[7]:


mean = np.arange(4, 8, 1)
mean_2 = np.arange(12, 16, 1)

DistNormal = {}
for i in range(len(mean)):
    desvpad = random.choice(np.arange(1, 3, 1))
    dNormal = np.random.normal(mean[i], desvpad, 100)

    desvpad_2 = random.choice(np.arange(2, 6, 1))
    dNormal_2 = np.random.normal(mean_2[i], desvpad_2, 100)
    DistNormal['Bimodal' + str(mean[i]) + str(mean_2[i]) + '_' + str(desvpad) + str(desvpad_2)] = []
    Joined = list(dNormal) + list(dNormal_2)
    DistNormal['Bimodal' + str(mean[i]) + str(mean_2[i]) + '_' + str(desvpad) + str(desvpad_2)].append(Joined)
    
    plt.rcParams["figure.figsize"] = [18, 5]

    distplot(DistNormal['Bimodal' + str(mean[i]) + str(mean_2[i]) + '_' + str(desvpad) + str(desvpad_2)], bins =  100,
             kde_kws={"color": "k", "lw": 5, "label": "Melhor Ajuste", 'linestyle': '--'},
             hist_kws={"histtype": "bar", "linewidth": 4, "alpha": 1, "color": "slategray", "rwidth": 0.6});
    plt.legend(fontsize = 16)
#     plt.title('Histograma para a distribuição {} com médias {} e {} e desvios padrões {} e {}'.format('MultiModal', mean[i], mean_2[i], desvpad, desvpad_2), fontsize= 22)
    plt.ylabel('Frequência (%)', fontsize = 20)
    plt.xlabel('Observação', fontsize = 20)
    plt.xticks(fontsize = 18)
    plt.yticks(fontsize = 18)
    plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\Distribution and Series\D_Bimodal' + str(mean[i]).replace(".",'') + str(mean_2[i]).replace(".",'') + '_' + str(desvpad) + str(desvpad_2) + '.png')
    plt.show()
    
dict_of_Normal = {k: pd.DataFrame(v) for k,v in DistNormal.items()}
Normal = pd.concat(dict_of_Normal, axis = 0).transpose()
Normal.columns = Normal.columns.levels[0]


# ### Concatanate the columns of all DF into a single DF

# In[8]:


Distributions = pd.concat([Paretos, 
                           Normal,
                           Weibull,
                           Burr,
                           LogNormal], axis = 1)

Distributions.to_excel(r"C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Data\Distributions\Distributions.xlsx")


# ### Randomly generates AR parameters to Series 'till order 6

# In[9]:


# ARParam = {}
# order = np.arange(1,5,1)

# for j in range(1,5):
# #     for i in range(1, 5):
#     a = random.sample(range(35, 85), j)
#     a = np.divide(a, 100)
#     ARParam['Ordem_' + str(j) + '_Param_' + str(a)] = []
#     ARParam['Ordem_' + str(j) + '_Param_' + str(a)].append(a)
        
# dict_of_Param = {k: pd.DataFrame(v) for k,v in ARParam.items()}
# ARParams = pd.concat(dict_of_Param, axis = 0).transpose()
# ARParams.columns = ARParams.columns.levels[0]
# ARParams.fillna(0, inplace = True)
ARParams = pd.DataFrame(columns = ['Ordem_1_Param_[0.57]', 
                                   'Ordem_2_Param_[0.76 0.52]', 
                                   'Ordem_3_Param_[0.79 0.41 0.68]',
                                   'Ordem_4_Param_[0.44 0.57 0.62 0.67]'])
ARParams['Ordem_1_Param_[0.57]'] = [0.57, 0, 0, 0]
ARParams['Ordem_2_Param_[0.76 0.52]'] = [0.76, 0.52, 0, 0]
ARParams['Ordem_3_Param_[0.79 0.41 0.68]'] = [0.79, 0.41, 0.68, 0]
ARParams['Ordem_4_Param_[0.44 0.57 0.62 0.67]'] = [0.44, 0.57, 0.62, 0.67]


# ### Generate The Series 'till 4th order, using the parameters generated in the cell above. Store The Parameters to add on the Written Work

# In[10]:


# maparams = [1, 0]
# col = ARParams.columns.values
# series = {}

# for i in range(len(col)):
#     arparams = ARParams[ARParams[col[i]] != 0][col[i]].values
#     ar = np.r_[1, arparams] # add zero-lag and negate
# #     print(ar)
# #     ma = np.r_[1, maparams] # add zero-lag
#     y = sm.tsa.arma_generate_sample(ar, maparams, 200)
#     series['Ordem_' + str(len(ar[1:])) + '_Param_' + str(ar[1:])] = []
#     series['Ordem_' + str(len(ar[1:])) + '_Param_' + str(ar[1:])].append(y)
    
# dict_of_Series = {k: pd.DataFrame(v) for k,v in series.items()}
# Series = pd.concat(dict_of_Series, axis = 0).transpose()
# Series.columns = Series.columns.levels[0]


# ### Applying Time Series to The Distributions

# In[11]:


# col_series    = Series.columns.values

# col_Pareto    = Paretos.columns.values
# col_LogNormal = LogNormal.columns.values
# col_Weibull   = Weibull.columns.values
# col_Burr      = Burr.columns.values
# col_Normal    = Normal.columns.values

# k = [col_Pareto,
#      col_LogNormal,
#      col_Burr,
#      col_Normal,
#      col_Weibull
#     ]

# for n in range(len(k)):
#     for j in range(len(col_series)):
        
#         for i in range(len(k[n])):
            
#             # Defining White noise
#             ARSeries = pd.DataFrame()
#             ARSeries[k[n][i] + '_' + col_series[j]] = Series[col_series[j]] + (Distributions[k[n][i]])
#             ARSeries.to_excel(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Data\Synthetic_Series\Serie_{}_Ordem_{}.xlsx'.format(k[n][i], col_series[j].split("_")[1]))
            
#             plt.rcParams["figure.figsize"] = [20, 5]   

#             ARSeries[k[n][i] + '_' + col_series[j]].plot(linewidth=2, color='black')
#             plt.title('Modelo AR({}) aplicado a uma Série Sintética {}{} {}'.format(col_series[j].split("_")[1],
#                                                                                                      k[n][i].split("_")[0],
#                                                                                                      np.where(k[n][i].split("_")[0] == 'MultiModal'," ",', com parâmetros de'),
#                                                                                                      np.where(k[n][i].split("_")[0] == 'MultiModal', " ", " ".join(k[n][i].split("_")[1:]))), fontsize = 20)
#             plt.ylabel('Valores Observados', fontsize = 16)
#             plt.xlabel('Número da Observação', fontsize = 16)
#             plt.xticks(fontsize = 14)
#             plt.yticks(fontsize = 14)
#             plt.xlim([0, len(ARSeries)])
#             plt.tight_layout()
#             plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\Serie_{}_Ordem_{}.png'.format(k[n][i].replace(".",'-'), col_series[j].split("_")[1]))
#             plt.show()
            
#             plt.rcParams["figure.figsize"] = [18, 5]

#             distplot(ARSeries[k[n][i] + '_' + col_series[j]], bins =  100,
#                      kde_kws={"color": "k", "lw": 5, "label": "Melhor Ajuste", 'linestyle': '--'},
#                      hist_kws={"histtype": "bar", "linewidth": 4, "alpha": 1, "color": "slategray", "rwidth": 0.6})
#             plt.legend(fontsize = 16)
#         #     plt.title('Histograma para a distribuição {} com médias {} e {} e desvios padrões {} e {}'.format('MultiModal', mean[i], mean_2[i], desvpad, desvpad_2), fontsize= 22)
#             plt.ylabel('Frequência (%)', fontsize = 20)
#             plt.xlabel('Observação', fontsize = 20)
#             plt.xticks(fontsize = 18)
#             plt.yticks(fontsize = 18)
#             plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\Histogram_Serie_{}_Ordem_{}.png'.format(k[n][i].replace(".",'-'), col_series[j].split("_")[1]))
#             plt.show()


# In[12]:


Orders    = ARParams.columns.values

col_Pareto    = Paretos.columns.values
col_LogNormal = LogNormal.columns.values
col_Weibull   = Weibull.columns.values
col_Burr      = Burr.columns.values
col_Normal    = Normal.columns.values

k = [col_Pareto,
     col_LogNormal,
     col_Burr,
     col_Normal,
     col_Weibull
    ]

for n in range(len(k)):
    for j in Orders:
        Parameters = ARParams[j].values
        Parameters = Parameters[Parameters != 0]
        for i in range(len(k[n])):
            # Sample of i.i.d White Noise Process with Mean = 0 And Standard Deviation = 1, with sample size = 200
            snWhiteNoise = np.random.normal(0, 1, size=200)
            Series = []
            Series.append(Distributions[k[0][1]][0])
            for m in range(1, len(Parameters)):
                Series.append(sum(Distributions[k[n][i]][:m].values * Parameters[:m]))
            for t in range(len(Parameters), 200):
                Series.append(sum(Distributions[k[0][1]][(t-len(Parameters)):t] * Parameters))
                
            ARSeries = pd.DataFrame()
            ARSeries[k[n][i] + '_' + j] = Series + snWhiteNoise
            ARSeries.to_excel(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Data\Synthetic_Series\Serie_{}_Ordem_{}.xlsx'.format(k[n][i], j.split("_")[1]))
            
            plt.rcParams["figure.figsize"] = [20, 5]   

            ARSeries[k[n][i] + '_' + j].plot(linewidth=2, color='black')
            plt.title('Modelo AR({}) aplicado a uma Série Sintética {}'.format(j.split("_")[1],
                                                                               findall('Pareto|LogNormal|Weibull|Burr|Bimodal', k[n][i])[0]), fontsize = 20)
            plt.ylabel('Valores Observados', fontsize = 16)
            plt.xlabel('Observação', fontsize = 16)
            plt.xticks(fontsize = 14)
            plt.yticks(fontsize = 14)
            plt.xlim([0, len(ARSeries)])
            plt.tight_layout()
            plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\Distribution and Series\S_{}_{}.png'.format(k[n][i].replace(".",'-'), j.split("_")[1]))
            plt.show()
            
            plt.rcParams["figure.figsize"] = [18, 5]

            distplot(ARSeries[k[n][i] + '_' + j], bins =  100,
                     kde_kws={"color": "k", "lw": 5, "label": "Melhor Ajuste", 'linestyle': '--'},
                     hist_kws={"histtype": "bar", "linewidth": 4, "alpha": 1, "color": "slategray", "rwidth": 0.6})
            plt.legend(fontsize = 16)
        #     plt.title('Histograma para a distribuição {} com médias {} e {} e desvios padrões {} e {}'.format('MultiModal', mean[i], mean_2[i], desvpad, desvpad_2), fontsize= 22)
            plt.ylabel('Frequência (%)', fontsize = 20)
            plt.xlabel('Valor', fontsize = 20)
            plt.xticks(fontsize = 18)
            plt.yticks(fontsize = 18)
            plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\Distribution and Series\HS_{}_{}.png'.format(k[n][i].replace(".",'-'), j.split("_")[1]))
            plt.show()


# In[ ]:


k[0][1]


# ### Creating MultiModal Series with a mix of all other distributions

# In[ ]:


# col_series    = Series.columns.values

# col_Pareto    = Paretos.columns.values
# col_LogNormal = LogNormal.columns.values
# col_Weibull   = Weibull.columns.values
# col_Burr      = Burr.columns.values
# col_Normal    = Normal.columns.values

# k = [col_Pareto,
#      col_LogNormal,
#      col_Burr,
#      col_Normal,
#      col_Weibull
#     ]

# for n in range(len(k)):
#     for j in range(len(col_series)):
#         for i in range(len(k[n])):
#             ARSeries = pd.DataFrame()
#             #ARSeries[col_series[j] + k[n][i]] = Series[col_series[j]] + (Distributions[k[n][i]])
#             col = [i for i in Series.columns if i != col_series[j]][np.random.randint(0,2)]
            
#             dAppendingData = pd.DataFrame((Series[col].values)+10, columns=[col_series[j] + k[n][i]])
#             array = Series[col_series[j]] + Distributions[k[n][i]]
#             ARSeries[col_series[j] + '+' + k[n][i] + '_Mix_' + col] = dAppendingData[dAppendingData.columns[0]].append(array, ignore_index=True)
#             ARSeries = ARSeries.sample(frac=1).reset_index().drop('index', axis = 1)
            
#             print(col_series[j] + '+' + k[n][i] + '_Mix_' + col)
            
#             plt. figure(figsize=(12,5))
#             plt.subplot(1, 3, 1)
#             plt.hist((Series[col].values)+5)
            
#             plt.subplot(1, 3, 2)
#             plt.hist(array)
            
#             plt.subplot(1, 3, 3)
#             plt.hist(ARSeries[col_series[j] + '+' + k[n][i] + '_Mix_' + col])
            
#             plt.show()
# #             ARSeries[col_series[j] + k[n][i]] = ARSeries[col_series[j] + k[n][i]].append(0)

#             writer = pd.ExcelWriter(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Time_Series\MultiModals\Synthetic_Series_{}.xlsx'.format(col_series[j] + '+' + k[n][i] + '_Mix_' + col), engine='xlsxwriter')
#             # Convert the dataframe to an XlsxWriter Excel object.
#             ARSeries.to_excel(writer)
#             # Close the Pandas Excel writer and output the Excel file.
#             writer.save()

