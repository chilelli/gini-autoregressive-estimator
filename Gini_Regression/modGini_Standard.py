
# coding: utf-8

# In[2]:


def FuncFindFiles(directory, ext):
    
    import os
    
    os.chdir(directory)

    # Add the path of the subject directory
    path = directory

    # this is the extension you want to detect
    extension = ext

    files = [] #List where the names will be stored

    #GET THE FILE NAMES IN A SPECIFIED DIRECTORY
    for root, dirs_list, files_list in os.walk(path):
        for file_name in files_list:
            if os.path.splitext(file_name)[-1] == extension:
                file_name_path = os.path.join(root, file_name)
                files.append(file_name)
                
    return(files)


def FuncPrintHeader(files):
    
    print('         =============================================================================')     
    print('')
    print('')
    print('')
    print('')
    print('                  ESTIMATION OF ARIMA COEFFICIENTS BY THE GINI METHODOLOGY TO A ')
    print('                  {} DISTRIBUTION'.format(files.split('.xlsx')[0].upper()))
    print('')
    print('')
    print('')
    print('')
    print('         =============================================================================')
    print('')

    
def FuncCreateMatrixRank(dMatrix_ranks, dData, sCol):
    
    from statsmodels.tsa.stattools import adfuller
    from pandas import Series
    from numpy import diff
    
    for f in range(len(sCol)):
        dMatrix_ranks[sCol] = dData[sCol] - dData[sCol].mean()
        Fuller = adfuller(dMatrix_ranks[sCol])
        
        if Fuller[4]['10%'] < Fuller[0]:
            dif = Series(diff(dMatrix_ranks[sCol], n=1))
            dMatrix_ranks[sCol][1:] = dif
            
    dMatrix_ranks.drop(dMatrix_ranks.index[0], inplace=True)
    return(dMatrix_ranks)
    
def FuncsiGACFOne(siGACF_1, dMatrix_ranks, sCol, iNLags, siGini_Covs):
    from numpy import cov
    
    for b in range(1,iNLags+1):
        dCumLag = (dMatrix_ranks[sCol][:-b].rank(method='average',na_option='top'))/len(dMatrix_ranks[sCol][:-b])
        nNumerator = cov(dMatrix_ranks[sCol][b:],dCumLag)[0][1]
        nDenominator = cov(dMatrix_ranks[sCol], dMatrix_ranks['CDF_' + sCol])[0][1]
        siGini_Covs[sCol].append(4*nNumerator)
        siGACF_1[sCol].append(nNumerator/nDenominator)
    return(siGACF_1, siGini_Covs)
    
def FuncsiGACFTwo(siGACF_2, dMatrix_ranks, sCol, iNLags, siGini_Covs):
    from numpy import cov
        
    for b in range(1,iNLags+1):
        dCumLag = (dMatrix_ranks[sCol][:-b].rank(method='average',na_option='top'))/len(dMatrix_ranks[sCol][:-b])
        nNumerator = cov(dMatrix_ranks[sCol][:-b],dMatrix_ranks['CDF_' + sCol][b:])[0][1]
        nDenominator = cov(dMatrix_ranks[sCol][:-b], dCumLag)[0][1]
        siGini_Covs[sCol].append(4*nNumerator)
        siGACF_2[sCol].append(nNumerator/nDenominator)
    return(siGACF_2, siGini_Covs)

def FuncCalcTheGiniCoefs(iNLags, siGACF, sCol, siPartial, siReverse):
    for t in range(2, iNLags+1):
            up = siGACF[sCol][t-1] - sum([c*d for c,d in zip(siPartial[t-1],siReverse[-(t-1):])])
            down = 1 - sum([c*d for c,d in zip(siPartial[t-1],siGACF[sCol][:(t-1)])])
            siPartial[t][t] = up/down   

            for j in range(1,t):
                siPartial[t][j] = siPartial[t-1][j] - siPartial[t][t] * siPartial[t-1][t-j]
    return(siPartial)

def FuncCoefGini(sCol, siGACF_1, siGACF_2):
    siCoef_Gar_1, siCoef_Gar_2 = ([] for x in range(2))
    
    siCoef_Gar_1.append(siGACF_1[sCol][0]) # Lag 1 Partial Autocorrelation
    siCoef_Gar_1.append((siGACF_1[sCol][1] - siGACF_1[sCol][0]**2)/(1 - siGACF_1[sCol][0]**2)) # Lag 2 Partial Autocorrelation
    
    siCoef_Gar_2.append(siGACF_2[sCol][0]) # Lag 1 Partial Autocorrelation
    siCoef_Gar_2.append((siGACF_2[sCol][1] - siGACF_2[sCol][0]**2)/(1 - siGACF_2[sCol][0]**2)) # Lag 2 Partial Autocorrelation
    
    return(siCoef_Gar_1, siCoef_Gar_2)


def FuncGiniCorrOne(snX, snY):
    
    from numpy import cov
    from pandas import Series
    
    dCumLagY = Series((snY.rank(method='average', na_option='top')/len(snY)) - 1/2)
    dCumLagX = Series((snX.rank(method='average', na_option='top')/len(snX)) - 1/2)

    nNumerator = cov(snX, dCumLagY)[0][1]
    nDenominator = cov(snX, dCumLagX)[0][1]
    nGiniCorrelation = nNumerator/nDenominator
    return(nGiniCorrelation)
    
def FuncGiniCorrTwo(snX, snY):
    from numpy import cov
    from pandas import Series
    
    dCumLagY = Series((snY.rank(method='average',na_option='top')/len(snY)) - 1/2)
    dCumLagX = Series((snX.rank(method='average',na_option='top')/len(snX)) - 1/2)
    
    nNumerator = cov(snY, dCumLagX)[0][1]
    nDenominator = cov(snY, dCumLagY)[0][1]
    nGiniCorrelation = nNumerator/nDenominator
    return(nGiniCorrelation)


# ### FINDING THE BEST ARIMA PARAMETERS

# In[2]:


def FuncFindBestARIMA(iPmin, iPmax, sCol, dMatrix_ranks, dData_central):
    
    from statsmodels.tsa.arima_model import ARIMA
    from itertools import product
    from numpy import arange
    
    dct = {}
    
    siP = arange(iPmin, iPmax, 1)
    d = q = [0]

    # Generate all different combinations of p, q and q triplets
    siPDQ = list(product(siP,d,q))
    
        
    #print('                       Finding Best Model to sColumn ------------>  ', sCol)
    dct[sCol] = [] #Creates a Dictionary to store the outputs.
    for siParam in siPDQ:
        try:
            model = ARIMA(dMatrix_ranks[sCol].dropna(), order = siParam)
            results = model.fit()
            dct[sCol].append({'P': siParam, 'AIC': results.aic})
#                 print('ARIMA{} - AIC:{}'.format(param, results.aic))
        except:
            continue
            
    #------------------------------------- TRANSFORME THE DICTIONARY INTO A DATAFRAME ----------------------------------#
    from pandas import concat
    from pandas import DataFrame
    
    dict_of_best_arima = {k: DataFrame(v) for k,v in dct.items()}
    dBestArima = concat(dict_of_best_arima, axis=1)
    
#     best_arima.rename(sColumns=sCol,inplace=True)
    
    #------------------------------ FIND THE LOWER AICs AND THE (P,D,Q) ASSOCIATED TO THEM -------------------------#
    siArrange = []
    siArrange.append(dBestArima[sCol]['AIC'].idxmin())
        
    dBestParam = []
    for append_best in range(len(siArrange)):    
        dBestParam.append(dBestArima[sCol]['P'][siArrange[append_best]])
    
    siForecast = []
    dFits = DataFrame()
    
    #------- RUN THE CLASSICAL ARIMA MODEL TO THE DATA BASED ON THE CHOSEN PARAMETERS -----------#
    model = ARIMA(dData_central[sCol].dropna(), order = dBestParam[0])
    model_fit = model.fit(trend='c')
    dFits['Res_'+ sCol] = model_fit.resid
    dFits['Fitted_'+ sCol] = model_fit.fittedvalues
    siForecast.append(model_fit.forecast(steps=1))
        
    return(dBestParam, siForecast, dFits, model_fit.arparams)


# ### GINI MINIMIZATION PURE AUTOREGRESSIVE MODEL FINDING

# In[22]:


def FuncGiniPureAR(dData_central, sCol, dBestParam, dMatrix_GAR_Coef1):
    
    from pandas import DataFrame, Series
    from numpy import arange, mean, zeros, empty, cov
    from scipy.optimize import minimize
    
    siForecasts = {}
    sDataAR = DataFrame()
    
        
    siForecasts[sCol] = []
    dDataGini = dData_central[sCol]

                       #========================================#
    #=================== PURE AUTOREGRESSIVE MODEL MINIMIZATION ======================#
                       #========================================#

    #                       THIS IS THE MINIMIZATION APPROACH               #

    if dBestParam[0][0] != 0:

        #=================== SAVE THE DATA THAT CAN BE FIT BY AN AR(p) MODEL==================#
        sDataAR[sCol] = dDataGini

        #================== FIND THE PARAMETERS ===============#
        #print('Pure AutoRegressive to: ', col)
        dDataGini.reset_index(drop=True,inplace=True)
#             print('order: ', dBestParam[0][0])
        dMatrix_of_Lags = DataFrame(index = dDataGini.index.values, columns = arange(1,dBestParam[0][0]+1,1))
        for p in range(1, dBestParam[0][0]+1): #Creates a Matrix where each column represents the data differenced by a lag K, starting on k = 1.
            dMatrix_of_Lags[p][(dBestParam[0][0]-p): -p] = dDataGini[(dBestParam[0][0]-p): -p] #The n# of columns is due to the order of this specific model.
        dMatrix_of_Lags = dMatrix_of_Lags.apply(lambda x: Series(x.dropna().values))
        sisiAR_Coeff = dMatrix_GAR_Coef1[sCol][:dBestParam[0][0]]

        error = Series()
        dDataGini = dDataGini[dBestParam[0][0]:]
        dDataGini = dDataGini.reset_index(drop=True)

        def f(params):
            rho = empty(dBestParam[0][0])
            rho = params # <-- for readability you may wish to assign names to the component variables
            error = dDataGini - Series.sum(dMatrix_of_Lags * rho, axis=1)
            rank_error = error.rank(method='average', na_option='top')/len(error)
            return cov(error, rank_error)[0][1]

        alpha = minimize(f, zeros(dBestParam[0][0]), method='BFGS', tol=1e-10, constraints={'type': 'eq', 'fun' : mean(error) - 0})

        for p in range(dBestParam[0][0]):
            siForecasts[sCol].append(0)
        siForecasts[sCol].extend(Series.sum(dMatrix_of_Lags*alpha.x[:],axis=1) )
            
        #------------------------ RESIDUALS  FROM THE GINI  -------------------------------#
        
    siResidualsGini = {}
    siResidualsGini['Residual' + sCol] = (dData_central[sCol][dBestParam[0][0]:] - siForecasts[sCol][dBestParam[0][0]:])
        
    return(siForecasts, sisiAR_Coeff, siResidualsGini)


# ### ERROR METRICS CALCULATION

# In[23]:


def FuncErrorMetrics(sCol, dData_central, dFits, dForecasts, dBestParam, dForecasts_SP):
    
    import pandas as pd
    from sklearn.metrics import mean_absolute_error,mean_squared_error
    
    siMAE, siMSE, siMAPE, siMSD = ({} for dic in range(4))
    
    siMAE['ARIMA_'+ sCol],   siMSE['ARIMA_'  + sCol], siMAPE['ARIMA_'  + sCol], siMSD['ARIMA_'  + sCol] = ([] for y in range(4))
    siMAE['GINI_M_'+ sCol],  siMSE['GINI_M_' + sCol], siMAPE['GINI_M_' + sCol], siMSD['GINI_M_' + sCol] = ([] for y in range(4))
    siMAE['GINI_SP_'+ sCol], siMSE['GINI_SP_'+ sCol], siMAPE['GINI_SP_'+ sCol], siMSD['GINI_SP_'+ sCol] = ([] for y in range(4))

    #-------------------------- ERROR METRICS FOR THE CLASSICAL ARIMA -------------------------#

    siMAE['ARIMA_'+sCol].append(mean_absolute_error(dData_central[sCol], dFits['Fitted_'+sCol]))
    siMSE['ARIMA_'+sCol].append(mean_squared_error(dData_central[sCol], dFits['Fitted_'+sCol]))
    #Mean Absolute Percentage Error (siMAPE)
    siMAPE['ARIMA_'+sCol].append((1/len(dFits))*sum(abs(((dData_central[sCol] - dFits['Fitted_'+sCol])/(dData_central[sCol])))))
    #Mean Squared Deviation
    siMSD['ARIMA_'+sCol].append((1/len(dFits))*sum(abs(dData_central[sCol] - dFits['Fitted_'+sCol]))**2)

    #----------------------------- ERROR METRICS FOR THE GINI MINIMIZATION------------------------------#

    siMAE['GINI_M_'+sCol].append(mean_absolute_error(dData_central[sCol][dBestParam[0][0]:], dForecasts[sCol][dBestParam[0][0]:]))
    siMSE['GINI_M_'+sCol].append(mean_squared_error(dData_central[sCol][dBestParam[0][0]:], dForecasts[sCol][dBestParam[0][0]:]))

    #Mean Absolute Percentage Error (siMAPE)
    siMAPE['GINI_M_'+sCol].append((1/len(dForecasts[sCol][dBestParam[0][0]:]))*sum(abs(((dData_central[sCol][dBestParam[0][0]:] - dForecasts[sCol][dBestParam[0][0]:])/
                                                                                       (dData_central[sCol][dBestParam[0][0]:])))))
    #Mean Squared Deviation
    siMSD['GINI_M_'+sCol].append((1/len(dForecasts[sCol][dBestParam[0][0]:]))*sum(abs(dData_central[sCol][dBestParam[0][0]:] - dForecasts[sCol][dBestParam[0][0]:]))**2)

    #----------------------------- ERROR METRICS FOR THE GINI SEMI PARAMETRIC------------------------------#

    siMAE['GINI_SP_'+sCol].append(mean_absolute_error(dData_central[sCol][dBestParam[0][0]:], dForecasts_SP[sCol][dBestParam[0][0]:]))
    siMSE['GINI_SP_'+sCol].append(mean_squared_error(dData_central[sCol][dBestParam[0][0]:], dForecasts_SP[sCol][dBestParam[0][0]:]))

    #Mean Absolute Percentage Error (siMAPE)
    siMAPE['GINI_SP_'+sCol].append((1/len(dForecasts_SP[sCol][dBestParam[0][0]:]))*sum(abs(((dData_central[sCol][dBestParam[0][0]:] - dForecasts_SP[sCol][dBestParam[0][0]:])/
                                                                                            (dData_central[sCol][dBestParam[0][0]:])))))
    #Mean Squared Deviation
    siMSD['GINI_SP_'+sCol].append((1/len(dForecasts_SP[sCol][dBestParam[0][0]:]))*sum(abs(dData_central[sCol][dBestParam[0][0]:] - dForecasts_SP[sCol][dBestParam[0][0]:]))**2)

    #----------------------------- ERROR METRICS FOR THE GINI ------------------------------#
    dError_metrics = pd.DataFrame.from_dict([siMAE, siMSE, siMAPE, siMSD])
    dError_metrics.index = ['MAE', 'MSE', 'MAPE', 'MSD']
    
    return(dError_metrics)


# ### GINI PURE AUTOREGRESSIVE SEMI-PARAMETRIC

# In[31]:


def FuncGiniARSemiParametric(dData_central, sCol, dBestParam, dMatrix_GAR_Coef1):
    
    from pandas import DataFrame, Series
    from numpy import arange
    
    dForecasts_SP = {}
    
    dForecasts_SP[sCol] = []
    dDataGini = dData_central[sCol]

    #================== FIND THE PARAMETERS ===============#
    dDataGini.reset_index(drop=True,inplace=True)
    dMatrix_of_Lags = DataFrame(index = dDataGini.index.values, columns = arange(1,dBestParam[0][0]+1,1))
    for p in range(1, dBestParam[0][0]+1): #Creates a Matrix where each column represents the data differenced by a lag K, starting on k = 1.
        dMatrix_of_Lags[p][(dBestParam[0][0]-p): -p] = dDataGini[(dBestParam[0][0]-p): -p] #The n# of columns is due to the order of this specific model.
    dMatrix_of_Lags = dMatrix_of_Lags.apply(lambda x: Series(x.dropna().values))
    siAR_Coeff = dMatrix_GAR_Coef1[sCol][:dBestParam[0][0]]

    for p in range(dBestParam[0][0]):
        dForecasts_SP[sCol].append(0)
    dForecasts_SP[sCol].extend(Series.sum(dMatrix_of_Lags*siAR_Coeff,axis=1))
        
    #------------------------ RESIDUALS  FROM THE GINI  -------------------------------#
    siResidualsGini = {}
    siResidualsGini['Residual'+sCol] = (dData_central[sCol][dBestParam[0][0]:] - dForecasts_SP[sCol][dBestParam[0][0]:])
    
    return(dForecasts_SP, siAR_Coeff, siResidualsGini)


# ### NORMALITY TEST

# In[45]:


def FuncNormalityTest(sCol, dData):
    
    from scipy.stats import shapiro, anderson
    
    #=================================== HISTOGRAM ===================================#
    print('-------------------------------------------------------------------------------------------')
    print('')
    print('--      ANALYZING: {}              '.format(sCol.upper()))
    print('')
    print('-------------------------------------------------------------------------------------------')
    print('')
    print('--------------- NORMALITY TEST ----------------')
    print('')

    Normality_Shapiro = shapiro(dData)
    if (Normality_Shapiro[1]<0.05):
        print('Shapiro Test:Os dados de '+sCol+ ' são Não Normais') # , pois o P-Value é {}, o que é menor do que 5%'.format(
#                 round(Normality_Shapiro[1],3)))
    else:
        print('Shapiro Test: Os dados de '+sCol+ ' são Normais') #, pois o P-Value é {}, o que é maior do que 5%'.format(
#                 round(Normality_Shapiro[1],3)))

    Normality_Anderson = anderson(dData)
    if (Normality_Anderson[0]>Normality_Anderson[1][2]):
        print('Anderson-Darling Test: Os dados de '+sCol+ ' são Não Normais')#, pois o Test Statistics é {}, o que é maior do que p-value de {} em 5%'.format(
#                 round(Normality_Anderson[0],3),round(Normality_Anderson[1][2],3)))
    else:
        print('Anderson-Darling Test: Os dados de '+sCol+ ' são Normais')#, pois o Test Statistics é {}, o que é menor do que p-value de {} em 5%'.format(
#                 round(Normality_Anderson[0],3),round(Normality_Anderson[1][2],3)))

    print('')
    print('')
#     return(Normality_Anderson)
    
def FuncNormalityResiduals(dFits, sCol):
    from scipy.stats import shapiro
    
    siResiduals_Shapiro = shapiro(dFits)
    if (siResiduals_Shapiro[1]<0.05):
        iIsnormal = 0
        print('Os resíduos de '+sCol+ ' são Não Normais')
    else:
        iIsnormal = 1
        print('Os resíduos de '+sCol+ ' são Normais')
        
    return(siResiduals_Shapiro, iIsnormal)  


# ### DATA DISTRIBUTION

# In[46]:


def FuncdDataDistribution(dData, bins):
    
    from seaborn import distplot
    from matplotlib.pyplot import legend, title, ylabel, xlabel, xticks, yticks, show, rcParams
    
    rcParams["figure.figsize"] = [18, 5]
        
    distplot(dData, bins = bins,
             kde_kws={"color": "k", "lw": 5, "label": "Melhor Ajuste", 'linestyle': '--'},
             hist_kws={"histtype": "bar", "linewidth": 4, "alpha": 1, "color": "slategray", "rwidth": 0.6});
    legend(fontsize = 16)
    title('dData Histogram to:  %s' %col, fontsize= 22)
    ylabel('Occurences', fontsize = 20)
    xlabel('dData (Centralized in the Mean)', fontsize = 20)
    xticks(fontsize = 18)
    yticks(fontsize = 18)
    show()


# ### PLOTTING AUTOCORRELATIONS

# In[47]:


def FuncPlotAutoCorrelations(dMatrix_ranks, iNLags, sCol, siGACF_1, siGACF_2, sOperator, sGoodOrBad):
    
    from matplotlib.pyplot import setp, tight_layout, legend, title, ylabel, xlabel, xticks, yticks, show, rcParams, subplot, stem, axhline, ylim, savefig, figure
    from statsmodels.tsa.stattools import acf
    from numpy import arange, sqrt
    
    param = ''.join(map(str, [int(s) for s in sCol.split("_")[:-2] if s.isdigit()]))
    name = sCol.split("_")[0] + param
    
    print('-------------------- AUTOCORRELATION --------------------')

    rcParams["figure.figsize"] = [20,8]
    
    subplot(1, 3, 1)
    markerline, stemlines, baseline = stem(acf(dMatrix_ranks,nlags = iNLags), markerfmt='kD', basefmt='k')
    setp(stemlines, 'color', 'black')
    xticks(arange(0,iNLags+1,1), arange(1,iNLags+1,1))
    title('ACF Clássico', fontsize = 16)
    ylabel('Auto Correlação', fontsize = 18)
    xlabel('Lags', fontsize = 18)
    xticks(fontsize = 10)
    yticks(fontsize = 14)
    
    subplot(1, 3, 2)
    markerline, stemlines, baseline = stem(acf(siGACF_1, nlags=iNLags), markerfmt='ks', basefmt='k')
    setp(stemlines, 'color', 'black')
    title('Gini ACF Operador Forward', fontsize = 16)
    xticks(arange(0,iNLags+1,1), arange(1,iNLags+1,1))
    xlabel('Lags', fontsize = 18)
    xticks(fontsize = 10)
    yticks(fontsize = 12)
    ylim(top=1.05)
    
    subplot(1, 3, 3)
    markerline, stemlines, baseline = stem(acf(siGACF_2, nlags = iNLags), markerfmt='ko', basefmt='k')
    setp(stemlines, 'color', 'black')
    xticks(arange(0,iNLags+1,1), arange(1,iNLags+1,1))
    title('Gini ACF Operador Backward', fontsize = 16)
    xlabel('Lags', fontsize = 18)
    xticks(fontsize = 10)
    yticks(fontsize = 12)
    ylim(top=1.05) 

    tight_layout

    savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\{}_ResultsImages_{}\GiniACF_{}.png'.format(sGoodOrBad, sOperator, name))
    show()


# ### PLOTTING PARTIAL AUTOCORRELATIONS

# In[48]:


def FuncPlotPartialAutocorr(dMatrix_ranks, iNLags, sCol, dMatrix_GAR_Coef1, dMatrix_GAR_Coef2, sOperator, sGoodOrBad):
    
    from matplotlib.pyplot import setp, tight_layout, legend, title, ylabel, xlabel, xticks, yticks, show, rcParams, subplot, stem, axhline, ylim, savefig, figure
    from statsmodels.tsa.stattools import pacf
    from numpy import arange, sqrt
    
    iParam = ''.join(map(str, [int(s) for s in sCol.split("_")[:-2] if s.isdigit()]))
    sName = sCol.split("_")[0] + iParam

    print('------------------ PARTIAL AUTOCORRELATION -----------------')
    
    rcParams["figure.figsize"] = [20,8]
    
    subplot(1, 3, 1)
    markerline, stemlines, baseline = stem(pacf(dMatrix_ranks,nlags = iNLags), markerfmt='kD', basefmt='k')
    setp(stemlines, 'color', 'black')
    xticks(arange(0,iNLags+1,1), arange(1,iNLags+1,1))
    title('PACF Clássico', fontsize = 16)
    ylabel('Auto Correlação Parcial', fontsize = 18)
    xlabel('Lags', fontsize = 18)
    xticks(fontsize = 10)
    yticks(fontsize = 14)

    subplot(1, 3, 2)
    markerline, stemlines, baseline = stem(pacf(dMatrix_GAR_Coef1, nlags=iNLags), markerfmt='ks', basefmt='k')
    setp(stemlines, 'color', 'black')
    title('Gini PACF Operador Forward', fontsize = 16)
    xticks(arange(0,iNLags+1,1), arange(1,iNLags+1,1))
    xlabel('Lags', fontsize = 18)
    xticks(fontsize = 10)
    yticks(fontsize = 12)
    ylim(top=1.05)

  
    subplot(1, 3, 3)
    markerline, stemlines, baseline = stem(pacf(dMatrix_GAR_Coef2, nlags = iNLags), markerfmt='ko', basefmt='k')
    setp(stemlines, 'color', 'black')
    xticks(arange(0,iNLags+1,1), arange(1,iNLags+1,1))
    title('Gini ACF Operador Backward', fontsize = 16)
    xlabel('Lags', fontsize = 18)
    xticks(fontsize = 10)
    yticks(fontsize = 12)
    ylim(top=1.05)
    
    savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\{}_ResultsImages_{}\GiniPACF_{}.png'.format(sGoodOrBad, sOperator, sName))
    show()


# ### Q-Q PROBABILITY PLOT AND RESIDUALS DISTRIBUTION

# In[49]:


def FuncPlotProbQQ(siResidual, siFitted, dData, sCol, sMethod, sOperator, sGoodOrBad):
    
    import matplotlib.pyplot as plt
    from seaborn import residplot
    import scipy.stats as stats
    from re import findall
    
    sSerieName = findall('LogNormal|Burr|Pareto|Weibull|Bimodal', sCol.split("_")[0])
    iParam = ''.join(map(str, [int(s) for s in sCol.split("_")[:-2] if s.isdigit()]))
    sName2 = sCol.split('_')[0] + iParam
    
    if sMethod.split(' ')[1] == 'Semi-Paramétrico':
        sName = 'SP'
    if sMethod.split(' ')[1] == 'Minimização':
        sName = 'Min'
    if sMethod.split(' ')[1] == 'Clássico':
        sName = 'C'
        
    plt.figure(figsize=(20, 8))

    plt.subplot(1, 2, 1)
    residplot(dData, siFitted, color = 'k')
    plt.title('{}: Gráfico Residual para Série {}'.format(sName, sSerieName), fontsize=20)
#     plt.title('{}: Gráfico Residual para Série {} e Ordem {}'.format(sName, ' '.join(sCol.split("_")[0:-4]), sCol.split("_")[-3]), fontsize=20)
    plt.ylabel('y', fontsize = 20)
    plt.xlabel('x', fontsize = 20)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 15)

    plt.subplot(1, 2, 2)
    stats.probplot(siResidual, dist="norm", plot=plt)
    plt.title("{}: Gráfico Q-Q Normal para Série {}".format(sName, sSerieName), fontsize=20)
#     plt.title("{}: Gráfico Q-Q Normal para {} e Ordem {}".format(sName, ' '.join(sCol.split("_")[0:-4]), sCol.split("_")[-3]), fontsize=20)
    plt.ylabel('Quartis Amostrais', fontsize = 16)
    plt.xlabel('Quartis Teóricos', fontsize = 16)
    plt.xticks(fontsize = 15)
    plt.yticks(fontsize = 15)

    plt.tight_layout()
    plt.savefig(r'C:\Users\SilvLu01\Documents\11 - Master\3 - Python\Gini\Gini_Process\Images\{}_ResultsImages_{}\{}_{}_QQ_{}.png'.format(sGoodOrBad, sOperator, sOperator[0], 
                                                                                                                                          sName, sName2))
    plt.show()


# In[ ]:


def fGiniCoefficient(array):
    import numpy as np
    """Calculate the Gini coefficient of a numpy array."""
    # based on bottom eq: http://www.statsdirect.com/help/content/image/stat0206_wmf.gif
    # from: http://www.statsdirect.com/help/default.htm#nonparametric_methods/gini.htm
    array = array.values
    array = array.flatten() #all values are treated equally, arrays must be 1d
    if np.amin(array) < 0:
        array -= np.amin(array) #values cannot be negative
    array += 0.0000001 #values cannot be 0
    array = np.sort(array) #values must be sorted
    index = np.arange(1,array.shape[0]+1) #index per array element
    n = array.shape[0]#number of array elements
    return ((np.sum((2 * index - n  - 1) * array)) / (n * np.sum(array))) #Gini coefficient

